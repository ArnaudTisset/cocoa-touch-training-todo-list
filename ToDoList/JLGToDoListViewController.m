//
//  JLGToDoListViewController.m
//  ToDoList
//
//  Created by Jlformation on 4/02/14.
//  Copyright (c) 2014 Jlformation. All rights reserved.
//

#import "JLGToDoListViewController.h"
#import "JLGToDoItem.h"
#import "JLGAddTodoItemViewController.h"

@interface JLGToDoListViewController ()
@property NSMutableArray *todoItems;
@property (strong, nonatomic) IBOutletCollection(UITableView) NSArray *itemList;
@end

@implementation JLGToDoListViewController

-(void)loadInitialData
{
    JLGToDoItem *item1 = [[JLGToDoItem alloc] init];
    item1.itemName = @"buy milk";
    [self.todoItems addObject:item1];
    
    JLGToDoItem *item2 = [[JLGToDoItem alloc] init];
    item2.itemName = @"CALL JOHN";
    [self.todoItems addObject:item2];
    
    
    JLGToDoItem *item3 = [[JLGToDoItem alloc] init];
    item3.itemName = @"tondre la pelouse";
    [self.todoItems addObject:item3];
    
    
    JLGToDoItem *item4 = [[JLGToDoItem alloc] init];
    item4.itemName = @"chercher les enfants à l'école";
    [self.todoItems addObject:item4];
    
    JLGToDoItem *item5 = [[JLGToDoItem alloc] init];
    item5.itemName = @"devenir millionnaire";
    [self.todoItems addObject:item5];
}

-(IBAction)unwindToList:(UIStoryboardSegue *)segue
{
    JLGAddTodoItemViewController * source = [segue sourceViewController];
    JLGToDoItem * item = source.toDoItem;
    if (item != nil) {
        [self.todoItems addObject:item];
        [self.tableView reloadData];
    }
}


- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.todoItems = [[NSMutableArray alloc] init];
    
    [self loadInitialData];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [self.todoItems count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"ListPrototypeCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    // Configure the cell...
    JLGToDoItem *todoItem =[self.todoItems objectAtIndex:indexPath.row];
    cell.textLabel.text = todoItem.itemName;
    
    if(todoItem.completed) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    } else {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    return cell;
}
- (IBAction)deleteItem1:(id)sender {
    

    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle: @"confirmation de suppression"message:@"Le voulez vous ?" delegate:self cancelButtonTitle:@"CANCEL" otherButtonTitles:@"OUI", nil];
                          
    [alert show];
                          
                          //:@"confirmation de suppression" message:@"voulez vous vraiment supprimer votre selection ?" delegate:nil cancelButtonTitle:@"non",nil, nil];

   // [self.todoItems removeObjectsInArray:toDelete];

    //
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    // the user clicked one of the OK/Cancel buttons
    if (buttonIndex == 0)
    {
        NSLog(@"cancel");
    }
    else
    {
        NSLog(@"OK");
        
        NSMutableArray * toDelete = [[NSMutableArray alloc] init];
        
        for(JLGToDoItem *i in self.todoItems)
        {
            if(i.completed)
            {
                [toDelete addObject:i];
            }
        }
        
        [self.todoItems removeObjectsInArray:toDelete];
        [self.tableView reloadData];
        
        
    }
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

 */

#pragma mark - Table view delegate

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:(NSIndexPath *)indexPath animated:NO];
    JLGToDoItem *tappedItem = [self.todoItems objectAtIndex:indexPath.row];
    tappedItem.completed = !tappedItem.completed;
    [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
}


@end
