//
//  JLGToDoItem.h
//  ToDoList
//
//  Created by Jlformation on 4/02/14.
//  Copyright (c) 2014 Jlformation. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JLGToDoItem : NSObject

@property NSString * itemName;
@property BOOL completed;
@property (atomic) NSDate * creationDate;

@end
