//
//  JLGAddTodoItemViewController.h
//  ToDoList
//
//  Created by Jlformation on 4/02/14.
//  Copyright (c) 2014 Jlformation. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JLGToDoItem.h"

@interface JLGAddTodoItemViewController : UIViewController
@property JLGToDoItem *toDoItem;
@end
