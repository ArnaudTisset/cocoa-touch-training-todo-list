//
//  main.m
//  ToDoList
//
//  Created by Jlformation on 4/02/14.
//  Copyright (c) 2014 Jlformation. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "JLGAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([JLGAppDelegate class]));
    }
}
